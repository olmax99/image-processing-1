package main

import (
	"errors"
	"flag"
	"fmt"
	"image"
	"log"
	"os"
	"strconv"
	"strings"
)

type Runner interface {
	Init([]string) error
	Run() error
	Name() string
}

// --------------- Create Random image-----------------------
func NewCreateRandomCommand() *CreateRandomCommand {
	crc := &CreateRandomCommand{
		fs: flag.NewFlagSet("random", flag.ContinueOnError),
	}

	crc.fs.StringVar(&crc.name, "name", "random.png", "name of the random image.")

	return crc
}

type CreateRandomCommand struct {
	fs   *flag.FlagSet
	name string
}

func (cr *CreateRandomCommand) Name() string {
	return cr.fs.Name()
}

func (cr *CreateRandomCommand) Init(args []string) error {
	return cr.fs.Parse(args)
}

func (cr *CreateRandomCommand) Run() error {
	rect := image.Rect(0, 0, 150, 150)
	randomImg(cr.name, rect)
	return nil
}

// ---------------- Create Color Histogram from *.jpg image---
func NewHistoCommand() *HistoCommand {
	hc := &HistoCommand{
		fs: flag.NewFlagSet("histo", flag.ContinueOnError),
	}

	return hc
}

type HistoCommand struct {
	fs *flag.FlagSet
}

func (hc *HistoCommand) Name() string {
	return hc.fs.Name()
}

func (hc *HistoCommand) Init(args []string) error {
	return hc.fs.Parse(args)
}

func (h *HistoCommand) Run() error {
	histo()
	return nil
}

// ---------------- flip an image-----------------------------
func NewFlipCommand() *FlipCommand {
	fc := &FlipCommand{
		fs: flag.NewFlagSet("flip", flag.ContinueOnError),
	}

	return fc
}

type FlipCommand struct {
	fs *flag.FlagSet
}

func (fc *FlipCommand) Name() string {
	return fc.fs.Name()
}

func (fc *FlipCommand) Init(args []string) error {
	return fc.fs.Parse(args)
}

func (fc *FlipCommand) Run() error {
	gr := load("1_E3zuxOrNORa-roJDDdw8ZA.jpeg")
	flip("1_E3zuxOrNORa-roJDDdw8ZA_flipped.jpeg", gr)
	return nil
}

// ---------------- grayscale an image--------------------------
func NewGrayscaleCommand() *GrayscaleCommand {
	fc := &GrayscaleCommand{
		fs: flag.NewFlagSet("grayscale", flag.ContinueOnError),
	}

	return fc
}

type GrayscaleCommand struct {
	fs *flag.FlagSet
}

func (fc *GrayscaleCommand) Name() string {
	return fc.fs.Name()
}

func (fc *GrayscaleCommand) Init(args []string) error {
	return fc.fs.Parse(args)
}

func (fc *GrayscaleCommand) Run() error {
	gr := load("1_E3zuxOrNORa-roJDDdw8ZA.jpeg")
	cc := grayscale(gr)
	save("1_E3zuxOrNORa-roJDDdw8ZA_gray.jpeg", cc)
	return nil
}

// ---------------- resize an image--------------------------
func NewResizeCommand() *ResizeCommand {
	rc := &ResizeCommand{
		fs: flag.NewFlagSet("resize", flag.ContinueOnError),
	}

	rc.fs.StringVar(&rc.scale, "scale", "1.0", "Scale applied to resize.")

	return rc
}

type ResizeCommand struct {
	fs    *flag.FlagSet
	scale string
}

func (rc *ResizeCommand) Name() string {
	return rc.fs.Name()
}

func (rc *ResizeCommand) Init(args []string) error {
	return rc.fs.Parse(args)
}

func (rc *ResizeCommand) Run() error {
	sf, err := strconv.ParseFloat(rc.scale, 64)
	if err != nil {
		log.Fatalf("Could not convert to float64.. %s", err)
	}
	gr := load("1_E3zuxOrNORa-roJDDdw8ZA.jpeg")
	cc := resize(gr, sf)
	save(strings.Join([]string{"1_E3zuxOrNORa-roJDDdw8ZA", rc.scale + ".jpeg"}, "_"), cc)
	return nil
}

// --------------- BASE CLI-----------------------------------
func root(args []string) error {
	if len(args) < 1 {
		return errors.New("You must pass a sub-command")
	}

	cmds := []Runner{
		NewCreateRandomCommand(),
		NewHistoCommand(),
		NewFlipCommand(),
		NewGrayscaleCommand(),
		NewResizeCommand(),
	}

	subcommand := os.Args[1]

	for _, cmd := range cmds {
		if cmd.Name() == subcommand {
			cmd.Init(os.Args[2:])
			return cmd.Run()
		}
	}

	return fmt.Errorf("Unknown subcommand: %s", subcommand)
}

func main() {
	if err := root(os.Args[1:]); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
