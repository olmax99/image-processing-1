package main

import (
	"crypto/rand"
	"fmt"
	"image"
	"image/color"
	"image/jpeg"
	"image/png"
	"log"
	"math"
	"os"
	"strings"
)

func save(filePath string, grid [][]color.Color) {
	xlen, ylen := len(grid), len(grid[0])
	rect := image.Rect(0, 0, xlen, ylen)
	img := image.NewNRGBA(rect)
	for x := 0; x < xlen; x++ {
		for y := 0; y < ylen; y++ {
			img.Set(x, y, grid[x][y])
		}
	}

	imgReader, err := os.Create(filePath)
	defer imgReader.Close()
	if err != nil {
		log.Println("Creating file failed..", err)
	}
	switch sf := strings.Split(filePath, "."); sf[1] {
	case "jpeg":
		q := &jpeg.Options{Quality: 80}
		jpeg.Encode(imgReader, img.SubImage(img.Rect), q)
	case "png":
		png.Encode(imgReader, img.SubImage(img.Rect))
	}

}

func load(filePath string) (grid [][]color.Color) {
	imgReader, err := os.Open(filePath)
	defer imgReader.Close()
	if err != nil {
		log.Println("Creating file failed..", err)
	}
	// returned as interface, needs type assert for initializing the underlying type
	m, _, err := image.Decode(imgReader)
	if err != nil {
		log.Println("Cannot decode file.")
	}
	size := m.Bounds().Size() // returns r's width and height.
	// [
	//   [(i0, j0), (i1, j1),         .. (ix, jx)], column0
	//   [(i100, j100), (i101, j101), .. (ix, jx)], column1
	//   [(i200, j200), (i201, j201), .. (ix, jx)], column2
	//   ..
	// ]
	for i := 0; i < size.X; i++ {
		var y []color.Color
		for j := 0; j < size.Y; j++ {
			y = append(y, m.At(i, j))
		}
		grid = append(grid, y)
	}
	return
}

func flip(filePath string, grid [][]color.Color) {
	var n [][]color.Color
	for x := 0; x < len(grid); x++ {
		col := grid[x]
		// only flip half of the rows => half of the grid column respectively
		for y := 0; y < len(col)/2; y++ {
			z := len(col) - y - 1
			col[y], col[z] = col[z], col[y]
		}
		n = append(n, col)
	}
	save(filePath, n)
}

func grayscale(grid [][]color.Color) (gray [][]color.Color) {
	xlen, ylen := len(grid), len(grid[0])
	gray = make([][]color.Color, xlen)
	for i := 0; i < len(gray); i++ {
		gray[i] = make([]color.Color, ylen)
	}

	for x := 0; x < xlen; x++ {
		for y := 0; y < ylen; y++ {
			pixl := grid[x][y].(color.YCbCr)
			gr := uint8(float64(pixl.Y)/3.0 + float64(pixl.Cb)/3.0 + float64(pixl.Cr)/3.0)
			gray[x][y] = color.YCbCr{gr, gr, gr}
		}
	}
	return
}

func resize(grid [][]color.Color, scale float64) (resized [][]color.Color) {
	xlen, ylen := int(float64(len(grid))*scale), int(float64(len(grid[0]))*scale)
	resized = make([][]color.Color, xlen)
	for i := 0; i < len(resized); i++ {
		resized[i] = make([]color.Color, ylen)
	}
	for x := 0; x < xlen; x++ {
		for y := 0; y < ylen; y++ {
			// Floor returns the greatest integer value less than or equal to x.
			xp := int(math.Floor(float64(x) / scale))
			yp := int(math.Floor(float64(y) / scale))
			resized[x][y] = grid[xp][yp]
		}
	}
	return
}

func randomImg(filePath string, rect image.Rectangle) {
	pixl := make([]uint8, rect.Dx()*rect.Dy()*4) // create a byte array of pixels
	rand.Read(pixl)                              // populate byte array with random numbers
	img := &image.NRGBA{
		// Pix holds the image's pixels, in R, G, B, A order. The pixel at
		// (x, y) starts at Pix[(y-Rect.Min.Y)*Stride + (x-Rect.Min.X)*4].
		Pix: pixl,
		// Stride is the Pix stride (in bytes) between vertically adjacent pixels.
		Stride: rect.Dx() * 4,
		// Rect is the image's bounds.
		Rect: rect,
	}

	imgReader, err := os.Create(filePath)
	defer imgReader.Close()
	if err != nil {
		log.Println("Creating file failed..", err)
	}
	png.Encode(imgReader, img.SubImage(img.Rect))

	return
}

func histo() {
	// Decode the JPEG data. If reading from file, create a reader with
	reader, err := os.Open("1_E3zuxOrNORa-roJDDdw8ZA.jpeg")
	if err != nil {
		log.Fatal(err)
	}
	defer reader.Close()

	m, _, err := image.Decode(reader)
	if err != nil {
		log.Fatal(err)
	}
	bounds := m.Bounds()

	// Calculate a 16-bin histogram for m's red, green, blue and alpha components.
	//
	// An image's bounds do not necessarily start at (0, 0), so the two loops start
	// at bounds.Min.Y and bounds.Min.X. Looping over Y first and X second is more
	// likely to result in better memory access patterns than X first and Y second.
	var histogram [16][4]int
	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			r, g, b, a := m.At(x, y).RGBA()
			// A color's RGBA method returns values in the range [0, 65535].
			// Shifting by 12 reduces this to the range [0, 15].
			// y >> z is "y divided by 2, z times"
			// => (y / (2^12)) == (y / 4096) => [0, 15.9998]
			histogram[r>>12][0]++
			histogram[g>>12][1]++
			histogram[b>>12][2]++
			histogram[a>>12][3]++
		}
	}

	// Print the results.
	fmt.Printf("%-14s %6s %6s %6s %6s\n", "bin", "red", "green", "blue", "alpha")
	for i, x := range histogram {
		// 0x  => hexadecimal number
		// %04 => fill with zeros, max four
		// x   => hexadecimal integer lowercase
		fmt.Printf("0x%04x-0x%04x: %6d %6d %6d %6d\n", i<<12, (i+1)<<12-1, x[0], x[1], x[2], x[3])
	}

}
